# Oktavy 22-23

GYMVOD programovací seminář Oktavy 2022/2023

## Učitelé

**David Hájek**

- hajekdd@gmail.com
- FEL
- Java, php, javascript, python


### TEST
24.02.2023 ze všeho!!

### Online



### Soutěž Kasiopea
- https://kasiopea.matfyz.cz/


### Pravidla

- Všichni se naučíme pracovat s GITem. Materiály budou na GitLabu.
- Discord - https://discord.gg/juk4bUZ4HQ
- Úkoly budou každý týden nebo ob týden. Na vypracování máte týden,
s dobrou výmluvou máte dva týdny. Jinak dostanete úkol navíc. Za
úkoly se budou dostávat jedničky. Neopisovat, poznám to. Odevzdávat
na svůj repositář.
- Budete mít projekt na semestr ve dvojicích. 
- Dvě až tři písemky za pololetí (teoretická a praktická část).
- Při hodině budete spolupracovat.

### Harmonogram 

| Datum | Učitel | Téma | Přednášky | Úkol |
| --- | --- | --- | --- | --- |
| 2.9.2022 | David | Úvod; seznámení; stránka; úkoly; semestrálka; postup při řešení problému; vlastnosti algoritmu, základní struktury; basic úloha | [lec01](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2022.09.02/uvodni_prezentace.pdf) | |
| 9.9.2022 | Suplování | Kalkulačka | [Samostatná práce](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2022.09.09/) | |
| 16.9.2022 | David | GIT, historie, lokální práce | [lec02](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2022.09.16/git-uvod.pdf) | |
| 23.9.2022 | David | Gitlab, kvadratická rovnice, Programovací jazyky  | [lec03](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2022.09.23/) | [hw01](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2022.09.23/hw)|
| 30.9.2022 | David | Datové typy | [lec04](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2022.09.30/) | [hw02](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2022.09.30/hw)|
| 7.10.2022 | David | Procvičování | [lec05](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2022.10.07/) | |
| 14.10.2022 | David | Základní struktury | [lec06](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2022.10.14/) | |
| 21.10.2022 | David | Cyklus a rekurze | [lec07](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2022.10.21/) | [hw03](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2022.10.21/hw) |
| 28.10.2022 | David | Volno |  | |
| 04.11.2022 | David | Rekurze a seznamy | [lec08](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2022.11.04/) | [hw04](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2022.11.04/hw) |
| 11.11.2022 | David | TEST |  | |
| 18.11.2022 | David | Volno | | |
| 25.11.2022 | David | String a stringBuilder | [lec09](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2022.11.25/) | [hw05](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2022.11.25/hw) |
| 02.12.2022 | David | ŘAZENÍ | [lec10](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2022.12.02/) | [hw06](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2022.12.02/hw) |
| 09.12.2022 | David | ŘAZENÍ + gui | [lec11](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2022.12.09/) | |
| 16.12.2022 | David | OOP | [lec12](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2022.12.16/) | [hw08](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2022.12.16/hw)  |
| 06.01.2023 | David | OOP 2 | [lec13](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2023.01.06/) |  |
| 13.01.2023 | David | Snowstorm | [lec14](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2023.01.13/) |  |
| 20.01.2023 | David | Eratosthen | [lec15](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2023.01.20/) |  |
| 27.01.2023 | David | Dictionary | [lec16](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2023.01.27/) |  |
| 17.02.2023 | David | Aritmetika | [lec17](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2023.02.17/) | [hw09](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2023.02.17/hw) |
| 24.02.2023 | David | MergeSort | [lec18](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2023.02.24/) |  |
| 03.03.2023 | David | Testicek | [lec19](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2023.03.03/) |  |
| 10.03.2023 | David | Morseovka | [lec20](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2023.03.10/) | [hw10](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2023.03.10/hw)  |
| 17.03.2023 | David | Grafika | [lec21](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2023.03.17/) | |
| 24.03.2023 | David | OpenFileDialog | [lec22](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2023.03.24/) | |
| 31.03.2023 | David | Zavorky | [lec23](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2023.03.31/) | |
| 14.04.2023 | David | Vektorovy obrazek | [lec24](https://gitlab.com/gymvod-group/oktavy-22-23/-/blob/main/lectures/2023.04.14/) | |













