﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace roman2
{
    class Converter : IRomanConverter
    {
        private readonly Dictionary<char, int> RomanNumber = new Dictionary<char, int>
        {
            {'I' , 1},
            {'V' , 5},
            {'X' , 10},
            {'L' , 50},
            {'C' , 100},
            {'D' , 500},
            {'M' , 1000},
        };

        private readonly Dictionary<int, string> NumberRoman = new Dictionary<int, string> 
        {
            { 1000, "M" },
            { 900, "CM" },
            { 500, "D" },
            { 400, "CD" },
            { 100, "C" },
            { 90, "XC" },
            { 50, "L" },
            { 40, "XL" },
            { 10, "X" },
            { 9, "IX" },
            { 5, "V" },
            { 4, "IV" },
            { 1, "I" },
        };

        public int FromRoman(string roman)
        {
            int total = 0;

            for (int i = 0; i < roman.Length; i++)
            {
                if (i > 0 && RomanNumber[roman[i]] > RomanNumber[roman[i-1]])
                    total += RomanNumber[roman[i]] - 2 * RomanNumber[roman[i - 1]];
                else
                    total += RomanNumber[roman[i]];
            }


            return total;
        }

        public string ToRoman(int number)
        {
            StringBuilder roman = new StringBuilder();

            foreach (KeyValuePair<int, string> item in NumberRoman)
            {
                while (number >= item.Key)
                {
                    roman.Append(item.Value);
                    number -= item.Key;
                }

            }


            return roman.ToString();
        }
    }
}
