﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace primes2
{
    class PrimeGenerator : IPrimeGenerator
    {
        private int lowerBound;
        private int upperBound;

        public PrimeGenerator(int lowerBound, int upperBound)
        {
            this.lowerBound = lowerBound;
            this.upperBound = upperBound;
        }

        public List<int> PrimesFromIntervalEratosthen()
        {
            //init
            bool[] sieve = new bool[upperBound + 1];
            for (int i = 2; i <= upperBound; i++)
            {
                sieve[i] = true;
            }

            //eratosthen
            double sqrt = Math.Sqrt(upperBound);
            for (int num = 2; num <= sqrt; num++)
            {
                if(sieve[num])
                {
                    for (int j = num*num; j <= upperBound; j += num)
                    {
                        sieve[j] = false;
                    }
                }
            }

            List<int> result = new List<int>();
            for (int k = lowerBound; k <= upperBound; k++)
            {
                if (sieve[k])
                    result.Add(k);
            }
            return result;
        }

        public List<int> PrimesFromIntervalOldWay()
        {
            List<int> result = new List<int>();

            for (int num = lowerBound; num <= upperBound; num++)
            {
                bool isPrime = true;
                double sqrt = Math.Sqrt(num);
                for (int i = 2; i <= sqrt; i++)
                {
                    if (num % i == 0)
                    {
                        isPrime = false;
                        break;
                    }
                }

                if (isPrime && num != 1)
                    result.Add(num);
            }

            return result;
        }
    }
}
