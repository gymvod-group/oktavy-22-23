﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatoveTypy
{
    class Program
    {

        private static string global = "globalni promenna";


        static void Main(string[] args)
        {

            //Datový typ definuje druh hodnot, kterých smí nabývat proměnná


            //Proměnnou deklarujeme pomocí datového typu a proměnné
            //Lokální proměnná
            //Používáme camelCase konvenci
            string variableName = "nazev";
            
            int number = 5;

            float flt = 1F / 3;
            float flt2 = (float) 1 / 3;
            double dbl = 1D / 3;
            decimal dcm = 1M / 3;
            Console.WriteLine("float: {0} double: {1} decimal: {2}", flt, dbl, dcm);
            Console.WriteLine(flt2);



            //Konverze datových typů
            //implicitní - vyhodnocování aritmetických výrazů
            int i = 10;
            long l = i;
            
            int s = 6;
            string message = "soucet je " + s;
            Console.WriteLine(message);
            Console.WriteLine(global);


            //Explicitní
            //Parsing, Třídu Convert, Explicitní cast operátor ()
            int parsedNumber = int.Parse("5");
            int convertedNumber = Convert.ToInt32("5");

            double doubleNumber = 5.6;
            int intNumber = (int) doubleNumber;
            Console.WriteLine(intNumber);

            //koverze na typ s menším rozsahem
            //vysledek je zbytek rozsahu cílové proměnné
            int hodnotaInt = 300;
            byte hodnotaByte = (byte) hodnotaInt;
            sbyte hodnotaSByte = (sbyte) hodnotaInt;
            Console.WriteLine();
            Console.WriteLine(hodnotaInt);
            Console.WriteLine(hodnotaByte);
            Console.WriteLine(hodnotaSByte);




            //Trida Convert hlida konverze;
            //Jde prevadet i nesourode datove typy;
            int intToConvert = 300;
            //int intToConvert = 255;
            try
            {
                byte bn = Convert.ToByte(intToConvert);
                Console.WriteLine();
                Console.WriteLine(bn);
            }
            catch (OverflowException e)
            {
                Console.WriteLine("Chytili jsme ji");
            }


            //checked
           // checked {

                byte val = 255;
                Console.WriteLine();
                Console.WriteLine("checked");
                Console.WriteLine(val++);
                Console.WriteLine(val);
                Console.WriteLine(++val);

            // }




            //Try & Catch si zapamtujte!
            try
            {
                checked
                {

                    int hodnotaIntChecked = 300;
                    byte hodnotaByteChecked = (byte)hodnotaIntChecked;

                }
            }
            catch (OverflowException e)
            {
                Console.WriteLine("Zase jsme ji chytili");
            }



            Console.WriteLine("A^2 + 2AB + B^2");


            Console.ReadKey();


        }
    }
}
