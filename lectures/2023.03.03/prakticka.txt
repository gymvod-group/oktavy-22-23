Na praktickou část máte čas do konce hodiny.



## 1. Objektově orientované programování
Máte deklarované rozhraní *ITvar* takto:
```
public interface ITvar
{
	decimal VypoctiObsah();
	string VratInformace();
}
```

Vytvořte třídy **Kruh, Ctverec a Obdelnik**, které implementují toto rozhraní. Čtverec je speciální případ obdélníku, proto by měl od něj dědit. Přepište pouze ty metody, které jsou nutné. Data potřebné pro výpočet obsahu ukládejte do vnitřního stavu a zajistěte, aby se naplnili při vytvoření instance. 

Každá třída bude mít
1) Atributy (vnitřní stav)
2) Konstruktor, který nasetuje atributy
3) Metody

Vytvořte libovolnou aplikaci, která 20x vytvoří náhodný z těchto objektů a vloží je do jednoho společného seznamu. Pro všechny tvary ze seznamu následně zavolejte obě metody a vypište uživateli výsledky.



