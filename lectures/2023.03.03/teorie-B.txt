B
Na vypracování teoretické části máte 35 minut.
Některé odpovědi se mohou lišit v jiných programovacích jazycích, ptám se na C# a "svět" .NET Frameworku.

Nematuranti nemusí na dvě otázky odpovědět.


a) Převody mezi binární a desítkovou soustavou. Zaznamenejte i postup, aby bylo zřejmé, jak jste k řešení dospěli. (5b)
	1) 163
	2) 100110111
	3) Jak dlouhé bude v binární soustavě číslo 119?

b) Podle čeho dělíme řadící algoritmy? Káždé kriterium vysvětlete a uveďte příklad algoritmu. (5b)

c) Vysvětlete algoritmy pracující na principu Rozděl a panuj a uvďte příklad algoritmu, jenž ho využívá. (5b)

d) Co znamenají zkratky CLI a GUI a jak se liší? Vysvětlete, kdy se hodí co. (5b)

e) Jaké znáte číselné soustavy? Výjmenujte 4 a řekněte, kde nebo k čemu se používají. (5b)

f) Co je to dictionary (slovník)? (5b)

g) Vysvětlete, co je to v OOP dědičnost. Proč vytváříme dědičnost? Kolikrát můžeme dědit? Jak v c# dědičnost vytvoříme? (5b)





