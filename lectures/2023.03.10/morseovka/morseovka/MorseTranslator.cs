﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace morseovka
{
    class MorseTranslator
    {
        private string alphabeticChars = "abcdefghijklmnopqrstuvwxyz";
        private string[] morseChars = { ".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.." };
        private char[] alphabet;
           
        public MorseTranslator()
        {
            alphabet = alphabeticChars.ToCharArray();
        }

        public string FromMorse(string input)
        {
            string[] inputStrings = input.Split(' ');
            string finalOut = "";
            foreach (string item in inputStrings)
            {
                int index = Array.IndexOf(morseChars, item);
                if (index >= 0)
                    finalOut += alphabet[index];
                else if (item == "")
                    finalOut += " ";
            }

            return finalOut;
        }

        public string ToMorse(string input)
        {
            char[] inputChars = input.ToCharArray();
            string finalOut = "";

            foreach (char item in inputChars)
            {
                int index = Array.IndexOf(alphabet, item);
                string outM;
                if (index >= 0)
                    outM = $"| {morseChars[index]} ";
                else if (item == ' ')
                    outM = "|  ";
                else
                    outM = "| @ ";

                finalOut += outM;
            }

            return finalOut + "|";
        }
    }
}
