# Binomická věta

Vytvořte konzolovou aplikaci, která vypočítá binomickou větu pro (A + B)^n. Uživatel zadá hodnotu n a program mu pak vypíše celou binomickou větu.
Hodnota n patří do množiny přirozených čísel.

### př.: 
	Pro (A + B)^n
	n = 2;
	
	A^2 + 2AB + B^2


### Deadline: 13.10.2021

### Doporučený postup:

1) Napište si funkci pro výpočet faktorialu.
2) Napište si funkci pro výpočet kombinačního čísla.
3) Obě funkce využíjte pro vytvoření hlavního algoritmu.
4) Pomáhejte si, ale kód pak upravte.
5) Používejte internet.
6) Napište mi, když bude problém.
